## gitlabamitrigger
This is a Lambda function that triggers a gitlab pipeline and passes it the latest AMI id for a given name. The lambda function can be triggered for example via SNS.
The code looks up the latest AMI and passes it to a gitlab project