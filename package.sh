#!/bin/bash

# package the lambda zip 

export OLDPWD=${PWD}
export AWS_DEFAULT_REGION=eu-west-1
export LAMBDA_NAME="gitlabamitrigger"
export LAMBDA_PATH=${OLDPWD}/amitrigger
mkdir -p .out .build
cp -r "${LAMBDA_PATH}" ".build"
pip install -r "${LAMBDA_PATH}/requirements.txt" -t "${OLDPWD}/.build/${LAMBDA_NAME}"
cd "${OLDPWD}/.build/${LAMBDA_NAME}" && zip -r "${OLDPWD}/${LAMBDA_NAME}.zip" .
cp "${OLDPWD}/$LAMBDA_NAME.zip" "${OLDPWD}/lambda-zip/"