import os
import boto3
import requests
import json
from dateutil import parser

def lambda_handler(event, context):

    # VARIABLES
    # Get Account Id from lambda function arn using passed in context
    ACCOUNT_ID = context.invoked_function_arn.split(":")[4]
    # Get environment variables
    LinuxFlavour = os.environ.get('LinuxFlavour') # pass your ami name
    GitlabCIProjectID = os.environ.get('GitlabCIProjectID')
    GitlabCIToken = os.environ.get('GitlabCIToken')
    GitlabCIBranch = os.environ.get('GitlabCIBranch')
    # Set proxy for boto3
    HTTP_PROXY  = os.environ.get('HTTP_PROXY') # if needed
    HTTPS_PROXY = os.environ.get('HTTPS_PROXY') # if needed
    # Configure no_proxy for requests
    NO_PROXY = os.environ.get('NO_PROXY') # if needed
    region='eu-west-1' # change to own liking
    # set function Variables
    # be aware you can set the proxy using client here. Its better for infra as code to get it as environment variable. Refer to the botocore docs for more information about the config property
    client = boto3.client('ec2', region_name=region)
    filters = [ {
            'Name': 'name',
            'Values': [LinuxFlavour + '*']
        },{
            'Name': 'owner-id',
            'Values': [ACCOUNT_ID]
        } ]

    # define get newest AMI method
    def newest_image(list_of_images):
        latest = None

        for image in list_of_images:
            if not latest:
                latest = image
                continue

            if parser.parse(image['CreationDate']) > parser.parse(latest['CreationDate']):
                latest = image

        return latest

    print("Linux type searched for is " + LinuxFlavour)
    print("Account ID=" + ACCOUNT_ID)
    response = client.describe_images(Owners=[ACCOUNT_ID], Filters=filters)
    source_image = newest_image(response['Images'])
    print('Latest ami is ' + source_image['ImageId'])

    # VARIABLES
    AMI_ID = source_image['ImageId']
    gitlabUrl = 'https://gitlab.com/api/v4/projects/' + GitlabCIProjectID + '/trigger/pipeline'
    print('Url called upon is ' + gitlabUrl)
    payload = { 'ref': GitlabCIBranch, 'token': GitlabCIToken, 'variables[AMI_ID]': AMI_ID }
    
    # Fire the trigger to VCS
    response = requests.post(
       gitlabUrl,
       data=payload, verify=False)  # please note that verify is set to false here

    print(response.json())
    return(response.json())
